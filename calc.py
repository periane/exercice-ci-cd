'''
La bibliothèque 'calc' contient la fonction 'add2' qui prend 2 valeurs et les addition ensemble. 
Si l'une des valeurs est une chaîne (ou les deux le sont), 'add2' verifie que
ce sont tous les deux des chaînes, ce qui donne un résultat concaténé.
NOTE: Si une valeur soumise à la fonction 'add2' est un float, il faut le faire
entre guillemets
'''

def conv(value):
    try:
        return int(value)
    except ValueError:
        try:
            return float(value)
        except ValueError:
            return str(value)

# la fonction 'add2'
def add2(arg1, arg2):
    # Convertir 'arg1' et 'arg2' en leurs types appropriés
    arg1conv = conv(arg1)
    arg2conv = conv(arg2)
    # Si «arg1» ou «arg2» est une chaîne, s'assurez qu'ils sont tous les deux des chaînes.
    if isinstance(arg1conv, str) or isinstance(arg2conv, str):
        arg1conv = str(arg1conv)
        arg2conv = str(arg2conv)
    return arg1conv + arg2conv
