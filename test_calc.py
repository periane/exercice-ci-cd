import unittest
import calc

class TestCalc(unittest.TestCase):
    """
    Teste de la fonction d'ajout de la bibliothèque calc
    """

    def test_add_integers(self):
        """
        Tester que l'addition de deux entiers renvoie le total correct
        """
        result = calc.add2(1, 2)
        self.assertEqual(result, 3)

    def test_add_floats(self):
        """
        Tester que l'ajout de deux flottants renvoie le résultat correct
        """
        result = calc.add2('10.5', 2)
        self.assertEqual(result, 12.5)

    def test_add_strings(self):
        """
        Tester que l'ajout de deux chaînes renvoie les deux chaînes comme une seule
        chaîne concaténée
        """
        result = calc.add2('abc', 'def')
        self.assertEqual(result, 'abcdef')

    def test_add_string_and_integer(self):
        """
        Tester que l'ajout d'une chaîne et un entier les renvoie comme une
        chaîne concaténée (dans laquelle l'entier est converti en chaîne)
        """
        result = calc.add2('abc', 3)
        self.assertEqual(result, 'abc3')

    def test_add_string_and_number(self):
        """
        Tester que l'ajout d'une chaîne et un flottant les renvoie comme une
        chaîne concaténée (dans laquelle le flottant est converti en chaîne)
        """
        result = calc.add2('abc', '5.5')
        self.assertEqual(result, 'abc5.5')

if __name__ == '__main__':
    unittest.main()
